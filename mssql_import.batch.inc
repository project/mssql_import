<?php

/************************************************
 * Constants
 */
 
define("MSSQL_IMPORT_MAX_ROWS", 35);


/************************************************
 * Batch callbacks.
 */
 
/**
 * Download the remote SQL Server database and save it into the local MySQL database.
 */
function _mssql_import_batch($args, &$context) {
  global $db_prefix;
  
  // Define the list of database tables to download
  $sql_tables = array(
    'table1',
  );
  $sql_tables_pri = array(
    'table1' => 'table1_primary_key',
  );
  
  // Load the current batch object
  $batch =& batch_get();
  if ($batch['error']) {  // Quit if there's a batch error
    $context['finished'] = 1;
    return;
  }
  
  // If this is the beginning of the batch, set the progress counters and clear the database tables.
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress']['table'] = 0;  // This will keep track of which table we're processing.
    $context['sandbox']['progress']['row'] = 0;  // This will keep track of which row we're on in the table.
    $context['sandbox']['progress']['num_rows'] = 0;  // This will keep track of how many rows are in the current table.
    
    // Drop all tables
    foreach ($sql_tables as $table) {
      if (db_result(db_query("SHOW TABLES LIKE '{".$args['options']['prefix'].$table."}'"))) {
        $result = @db_query('DROP TABLE {'.$args['options']['prefix'].$table.'}');
        if (!$result) {
          batch_set_error('Error: Could not drop the table: '.$db_prefix.$args['options']['prefix'].$table);
          return;
        }
      }
    }
  }
  
  // Initialize the database connection if it isn't already.
  if (!is_resource($context['sandbox']['db']['handle'])) {
    // First make sure PHP has the MS SQL functions
    if (!function_exists('mssql_connect')) {
      batch_set_error('Error: MS SQL PHP extension is not installed.');
      return;
    }
    
    // Connect to the SQL Server database
    $context['sandbox']['db']['handle'] = @mssql_connect($args['server']['alias'], $args['server']['user'], $args['server']['pass']);
    if (!$context['sandbox']['db']['handle']) {
      batch_set_error('Error: Couldn\'t connect to the server: '.$args['server']['alias']);
      return;
    }
    else {
      // Select the database
      $dbselected = @mssql_select_db($args['server']['database'], $context['sandbox']['db']['handle']);
      if (!$dbselected) {
        batch_set_error('Error: Couldn\'t select the database: '.$args['server']['database']);
        return;
      }
    }
  }
  
  // If we are not done with all of the tables yet...
  if ($context['sandbox']['progress']['table'] < count($sql_tables)) {
    
    // Set the table name and primary column
    $table = $sql_tables[$context['sandbox']['progress']['table']];
    $table_pri = $sql_tables_pri[$table];
    
    // If the table doesn't exist already, create it
    if (!db_result(db_query("SHOW TABLES LIKE '{".$args['options']['prefix'].$table."}'"))) {
      
      // Query the SQL Server for a description of the database fields
      $query = "sp_columns $table";
      $result = @mssql_query($query);
      if (!$result) {
        batch_set_error('Error: couldn\'t access the table: '.$table);
        return;
      }
      while ($field = @mssql_fetch_array($result, MSSQL_ASSOC)) if ($field) $fields[] = $field;
      
      // Generate a CREATE TABLE query
      $query = "CREATE TABLE `{".$args['options']['prefix']."$table}` (";
      $i = 0;
      foreach ($fields as $field) {
        $i++;
        $query .= "`".$field['COLUMN_NAME']."` ";
        switch ($field['DATA_TYPE']) {
          case -1: 
            $query .= "longtext";
            break;
          case 4:  // int
            $query .= "int(11) signed NOT NULL";
            break;
          case 6:  // float
            $query .= "float default NULL";
            break;
          case 12:  // varchar
            $query .= "varchar(".$field['LENGTH'].") NOT NULL";
            break;
        }
        // Append a space and maybe a comma:
        if ($i < count($fields)) $query .= ", ";
        else $query .= " ";
      }
      $query .= ") ENGINE=MyISAM DEFAULT CHARSET=utf8;";
      
      // Create the table
      $result = db_query($query);
      if (!$result) {
        batch_set_error('Error: couldn\'t create the \''.$db_prefix.$args['options']['prefix'].$table.'\' table.');
        return;
      }
    }
    
    // Count the number of rows in the table and store it for later use
    if (empty($context['sandbox']['progress']['num_rows'])) {
      $query = "SELECT COUNT(*) FROM ".$table;
      $result = @mssql_query($query);
      if (!$result) {
        batch_set_error('Error: couldn\'t count the number of rows in the table: '.$table);
        return;
      }
      $row = @mssql_fetch_array($result, MSSQL_ASSOC);
      $context['sandbox']['progress']['num_rows'] = $row['computed'];
    }
    
    // If we are not done with all the rows of this table yet
    $sqlrow = $context['sandbox']['progress']['row'] + 1;  // Convert array-style count to SQL-row-style
    if ($sqlrow <= $context['sandbox']['progress']['num_rows']) {
      
      // Query the database for the next set of rows
      $query = "SELECT TOP ".MSSQL_IMPORT_MAX_ROWS." * FROM ".$table." WHERE ".$table_pri." NOT IN (SELECT TOP ".$context['sandbox']['progress']['row']." ".$table_pri." FROM ".$table." ORDER BY ".$table_pri.")";
      $result = @mssql_query($query);
      if (!$result) {
        batch_set_error('Error: couldn\'t select rows '.$sqlrow.' to '.($sqlrow+MSSQL_IMPORT_MAX_ROWS).' from the table: '.$table);
        return;
      }
      
      while ($row = @mssql_fetch_array($result, MSSQL_ASSOC)) {
        $rows[] = $row;
      }
      
      // Loop through each row and insert it into the local database
      foreach ($rows as $row) {
        
        // Build the list of keys and values
        $keys = $values = '';
        foreach ($row as $key => $value) {
          $keys[] = $key;
          $values[] = "'".mysql_real_escape_string(utf8_encode($value))."'";  // For MySQL Insert, values need to be in quotes and properly escaped, and encoded to utf-8
        }
        
        // Build and execute the query
        $query = "INSERT INTO {".$args['options']['prefix'].$table."} (".implode(', ', $keys).") VALUES (".implode(', ', $values).")";
        $result = db_query($query);
        if (!$result) {
          batch_set_error('Error: couldn\'t insert row ('.$table_pri.' = '.$row[$table_pri].') into \''.$db_prefix.$args['options']['prefix'].$table.'\' table.');
          return;
        }
        
        // Increment row counter
        $context['sandbox']['progress']['row']++;
      }
      
      // Update the batch api on our progress
      $context['message'] = 'Copied '.$context['sandbox']['progress']['row'].'/'.$context['sandbox']['progress']['num_rows'].' rows from the table "'.$table.'"';
    }
    // If we ARE done with all the rows in this table, increment that table counter
    else {
      $context['sandbox']['progress']['table']++;  // Increment table counter
      $context['sandbox']['progress']['row'] = $context['sandbox']['progress']['num_rows'] = 0;  // Reset row counters
      $context['message'] = 'Copied the table "'.$table.'" from the remote database.';
    }
    
    // Tell the batch engine what our progress is
    $context['finished'] = $context['sandbox']['progress']['table'] / count($sql_tables);
  }
  
  // If we are done...
  else {
    // Print a message and finish
    $context['message'] = 'Finished copying database tables from remote server.';
    $context['finished'] = 1;
  }
}

/**
 * Batch finish.
 */
function _mssql_import_batch_finish($success, $results, $operations) {
  
  // Print batch errors, if any
  batch_print_errors();
  
  // If the batch completed successfully with a code 200
  if ($success) {
    // Theme the messages
    if (!empty($results['messages'])) $message .= theme('item_list', $results['messages']);
  }
  // If an HTTP error occurred...
  else {
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array('%error_operation' => $error_operation[0], '@arguments' => print_r($error_operation[1], TRUE)));
  }
  
  // Print out the messages
  drupal_set_message($message);
}